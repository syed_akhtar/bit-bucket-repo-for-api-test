const request = require('request');
const fs = require('fs');

var inStringLTO = '';           //string of all commits in latest to oldest order
var cronologicalStringOTL = ''; //string of all commits in oldest to latest order

request('https://api.bitbucket.org/2.0/repositories/calanceus/api-test/commits', { json: true }, (err, res, body) => {
  if (err) { return console.log(err); }
  console.log('API response recived');

  //loop through all possible commit messages
  for (var i = 0; true; i++){ 
    try{

      //grabbing messages from commits and concatinating them in OLT TLO order
      inStringLTO += body.values[i].message + '\r\n' ;
      cronologicalStringOTL = body.values[i].message + '\r\n' + cronologicalStringOTL;
    }
    
    //when body.values[i].message returns an error
    catch{
      break;     
    }  
  }

  //writing latest to oldest order to a file
  fs.writeFile("commitsLTO.txt", inStringLTO, function(err) { 
    if(err) {
      return console.log(err);
    }
    fs.close;
    console.log("The file (commitsLTO) was saved!");
  });
  
  //writing oldest to latest order to a file (cronologial)
  fs.writeFile("commitsOTL.txt", cronologicalStringOTL, function(err) {
    if(err) {
      return console.log(err);
    }
    fs.close;
    console.log("The file (commitsOTL) was saved!");
  }); 
});


